import { useCookies } from "react-cookie";
import jwt_decode, {JwtPayload} from "jwt-decode";


export type MemberInfo = {
    roomId: string,
    clientId: string,
    roles: Array<string>,
};

type ClientToken = JwtPayload & {
    roomMap: any,
};

export const useMemberInfo = (roomId: string): MemberInfo | null => {
    const [cookies] = useCookies(["clienttoken"]);
    const token = jwt_decode<ClientToken>(cookies.clienttoken)
    const info = token.roomMap[roomId]
    return info || null;
};