import { Outlet, Link, useNavigate} from "react-router-dom";
import "./navbar.css";

type LayoutProps = {
  loggedIn: boolean;
  logoutCallback: () => void;
}

const Layout = ({loggedIn, logoutCallback}: LayoutProps) => {
  const navigate = useNavigate();

  const logoutAction = () => {
    logoutCallback();
    navigate("/");
  }

  let usercontrol;
  if (loggedIn) {
    usercontrol = <span onClick={logoutAction}>Logout</span>
  } else {
    usercontrol = <Link to="/login">Login</Link>
  }

  return (
    <div>
      <nav>
        <ul className="navList">
          <li>
            <Link to="/">Home</Link>
          </li>
          {loggedIn &&
          <li>
            <Link to="/rooms">Rooms</Link>
          </li>
          }
          <li style={{ float: "right" }}>
            {usercontrol}
          </li>
        </ul>
      </nav>

      <Outlet />
    </div>
  )
};

export default Layout;