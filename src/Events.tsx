
export const ChangePrompt = "ChangePrompt";
export type ChangePromptEvent = {
    promptIndex: number;
}

export const JoinedRoom = "JoinedRoom";
export type JoinedRoomEvent = {
    clientId: string;
}

export const BuzzerPressed = "BuzzerPressed";
export const BuzzerReset = "BuzzerReset";
export const BuzzerAccepted = "BuzzerAccepted";

export const ChangePoints = "ChangePoints";
export type ChangePointsEvent = {
    change: number;
}

export const PlayerState = "PlayerState";
export type PlayerStateEvent = {
    clientId: string;
    points: number;
    onStage: boolean;
    buzzedIn: boolean;
}

export const RoomState = "RoomState";
export type RoomStateEvent = {
    promptIndex: number;
    players: Array<PlayerStateEvent>;
}

export const SetStagePresence = "SetStagePresence";
export type SetStagePresenceEvent = {
    onStage: boolean;
}

export const EndGame = "EndGame";

export type EventMessage = {
    type: string;
    subject: string,
    event?: any;
}
