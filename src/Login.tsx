import {useState} from 'react'
import { useNavigate } from 'react-router-dom';
import axios, { AxiosError } from 'axios';
import "./login.css";

type Credentials = {
    username: string,
    password: string,
}

type LoginPageProps = {
    loginCallback: () => void;
}

const LoginPage = ({loginCallback}: LoginPageProps) => {
    const navigate = useNavigate();
    const [credentials, setCredentials] = useState<Credentials>({ username: "", password: "" });
    const [errorMessage, setErrorMessage] = useState("");
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const name = event.target.name;
        const value = event.target.value;
        setCredentials(values => ({ ...values, [name]: value }))
    }
    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const sendCreds = async () => {
            try {
                await axios.post("/api/login", credentials);
                loginCallback();
                navigate(-1);
            } catch (err: AxiosError | any) {
                if (axios.isAxiosError(err)) {
                    let message = "Error Response " + err.response?.statusText;
                    switch (err.response?.status) {
                        case 401:
                            message = "Invalid username/password";
                            break;
                        case 403:
                            message = "Authorization failure";
                            break;
                        case 500:
                            message = "Server Error";
                            break;
                    }
                    setErrorMessage(message);
                } else {
                    console.error(err)
                    setErrorMessage("Problem performing login");
                }
            }
        };
        sendCreds();
    }

    return (
        <div>
            <h2 className="loginTitle">Enter Host Credentials</h2>
            <div className="login">
                <form id="login" onSubmit={handleSubmit}>
                    <label className="loginLabel"><b>Username</b>
                        <input type="text" id="Uname" name="username" value={credentials.username}
                            onChange={handleChange} />
                        <br /><br />
                    </label>
                    <label className="loginLabel"><b>Password</b>
                        <input type="password" id="Pass" name="password" value={credentials.password}
                            onChange={handleChange} />
                        <br /><br />
                    </label>
                    <input type="submit" name="log" id="log" value="Login" />
                    {errorMessage && <p style={{ color: 'red' }}>{errorMessage}</p>}
                </form>
            </div>
        </div>
    );
};

export default LoginPage;
