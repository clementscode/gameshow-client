import React from 'react';
import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { Game, Prompt, useGameSocket, useGameData, Player, applyPlayerState, withUpdate, withUpdates, PlayerMap } from './GameData';
import * as events from './Events'
import "./gameboard.css";
import ding from "./ding.wav";

const MAX_PLAYERS_ON_BOARD = 5;

type DisplayState = {
  currentPrompt: number;
  players: PlayerMap;
  gameOver: boolean;
}

const findWinners = (state: DisplayState): Array<Player> => {
  let highScore = 0;
  let results: Array<Player> = [];
  state.players.forEach((v: Player, k: string) => {
    if (v.points > highScore) {
      results = [v];
      highScore = v.points;
    } else if ( v.points == highScore) {
      results.push(v);
    }
  });
  return results;
};

const getPlayersForBoard = (state: DisplayState): Array<Player> => {
  let buzzIndex = -1;
  let results: Array<Player> = Array.from(state.players.values()).filter((item: Player) => item.onStage && (item.buzzedIn || item.points > 0));
  results = results.sort((a: Player, b: Player) => {
    let diff = b.points - a.points;
    return diff !== 0 ? diff : a.name.localeCompare(b.name);
  });
  if (results.length > MAX_PLAYERS_ON_BOARD) {
    results.forEach((item: Player, index: number) => {
      if (item.buzzedIn) {
        buzzIndex = index;
      }
    });
    if (buzzIndex >= results.length - 1) {
      results[MAX_PLAYERS_ON_BOARD - 1] = results[buzzIndex];
    }
    results = results.slice(0, MAX_PLAYERS_ON_BOARD);
  }
  return results;
};

const GameDisplay = () => {
  const { roomId } = useParams();

  const [game, isLoading, errorMessage] = useGameData(roomId || null);

  const [displayState, setDisplayState] = useState<DisplayState>({
    currentPrompt: -1,
    players: new Map<string, Player>(),
    gameOver: false,
  });

  const [isConnected, sendJsonMessage, lastJsonMessage] = useGameSocket(
    roomId ? roomId : "", "display"
  );

  useEffect(() => {
    if (lastJsonMessage != null) {
      let message = lastJsonMessage as events.EventMessage;
      switch (message.type) {
        case events.RoomState: {
          let stateEvent = message.event as events.RoomStateEvent;
          setDisplayState({
            ...displayState,
            currentPrompt: stateEvent.promptIndex,
            players: applyPlayerState(displayState.players, stateEvent.players),
          });
          break;
        }
        case events.ChangePrompt: {
          let change = message.event as events.ChangePromptEvent;
          setDisplayState({
            ...displayState,
            currentPrompt: change.promptIndex,
          });
          break;
        }
        case events.SetStagePresence: {
          let presence = message.event as events.SetStagePresenceEvent;
          setDisplayState({
            ...displayState,
            players: withUpdate(displayState.players, message.subject, (player) => {
              player.onStage = presence.onStage;
            }),
          });
          break;
        }
        case events.BuzzerAccepted: {
          setDisplayState({
            ...displayState,
            players: withUpdate(displayState.players, message.subject, (player) => {
              player.buzzedIn = true;
            }),
          });
          new Audio(ding).play();
          break;
        }
        case events.BuzzerReset: {
          setDisplayState({
            ...displayState,
            players: withUpdates(displayState.players, (player) => {
              player.buzzedIn = false;
            }),
          });
          break;
        }
        case events.ChangePoints: {
          let points = message.event as events.ChangePointsEvent;
          setDisplayState({
            ...displayState,
            players: withUpdate(displayState.players, message.subject, (player) => {
              player.points += points.change;
            }),
          });
          break;
        }
        case events.EndGame: {
          setDisplayState({
            ...displayState,
            gameOver: true,
          });
          break;
        }
        case events.JoinedRoom: {
          let player = { name: message.subject, points: 0, onStage: true, buzzedIn: false };
          setDisplayState({
            ...displayState,
            players: new Map(displayState.players).set(player.name, player)
          });
          break;
        }
        default: {
          console.log("Unhandled event: " + message.type);
        }
      }
    }
  }, [lastJsonMessage])

  const updatePrompt = (promptIndex: number) => {
    sendJsonMessage({
      type: events.ChangePrompt,
      event: {
        promptIndex: promptIndex
      }
    });
  };

  let boardContent;
  if (isLoading) {
    boardContent = <p className="boardPrompt">Loading...</p>
  } else if (displayState.gameOver) {
    let winners = findWinners(displayState);
    if (winners.length > 1) {
      let names = Array.from(winners, (w) => w.name).join(', ');
      boardContent = <p className="boardPrompt">Winners<br /><br />{names}</p>
    } else if (winners.length == 1) {
      boardContent = <p className="boardPrompt">Winner<br /><br />{winners[0].name}</p>
    } else {
      boardContent = <p className="boardPrompt">Game Over</p>
    }
  } else if (displayState.currentPrompt < 0) {
    boardContent = <p className="boardPrompt">{window.location.protocol}//{window.location.host}<br/>Room Number: {roomId}</p>
  } else {
    boardContent = <p className="boardPrompt">{game.prompts[displayState.currentPrompt].text}</p>
  }

  return (
    <div>
      {errorMessage && <p>{errorMessage}</p>}
      <div className="boardBorder">
        <div className="boardBack">
          {boardContent}
        </div>
      </div>
      <ScoreBoard players={getPlayersForBoard(displayState)} />
    </div>
  );
};

type ScoreBoardProps = {
  players: Array<Player>;
}

const ScoreBoard = ({ players }: ScoreBoardProps) => (
  <div className="scoreBoard">
    <ul className="boardPlayerList">
      {players.map((item) => (
        <PlayerScore
          key={item.name}
          item={item}
        />
      ))}
    </ul>
  </div>
);

type PlayerScoreProps = {
  item: Player;
};

const PlayerScore = ({ item }: PlayerScoreProps) => {

  let borderClass = "boardPlayerBorder";
  if (item.buzzedIn) {
    borderClass += " boardPlayerBuzzed";
  }
  return (
    <li className="boardPlayerListItem">
      <div className="boardPlayerBuffer">
        <div className={borderClass}>
          <div className="boardPlayerBack">
            <h1 className="boardPlayerNameText">{item.name}</h1>
            <div className="boardPlayerPointsBorder">
              <div className="boardPlayerPointsBack">
                <div>
                  <h1 className="boardPlayerPointsText">{item.points}</h1>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </li>
  );
};
export default GameDisplay;