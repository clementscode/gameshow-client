import React from 'react';
import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { Prompt, Prompts, Game, useGameData, useGameSocket } from './GameData';
import * as events from './Events'
import "./podium.css";

type PlayerState = {
  clientId: string,
  buzzedIn: boolean,
  points: number,
};

const Podium = () => {
  const { roomId } = useParams();

  const [isConnected, sendJsonMessage, lastJsonMessage] = useGameSocket(
    roomId ? roomId : "", "player"
  );

  const [playerState, setPlayerState] = useState({
    clientId: "",
    buzzedIn: false,
    points: 0,
  });

  useEffect(() => {
    if (lastJsonMessage != null) {
      let message = lastJsonMessage as events.EventMessage;
      switch (message.type) {
        case events.PlayerState: {
          let stateEvent = message.event as events.PlayerStateEvent;
          setPlayerState({
            ...playerState,
            clientId: stateEvent.clientId,
            buzzedIn: stateEvent.buzzedIn,
            points: stateEvent.points,
          });
          break;
        }
        case events.BuzzerAccepted: {
          if (playerState.clientId == message.subject) {
            setPlayerState({
              ...playerState,
              buzzedIn: true,
            });
          }
          break;
        }
        case events.BuzzerReset: {
          setPlayerState({
            ...playerState,
            buzzedIn: false,
          });
          break;
        }
        case events.ChangePoints: {
          let event = message.event as events.ChangePointsEvent;
          if (playerState.clientId == message.subject) {
            setPlayerState({
              ...playerState,
              points: playerState.points + event.change,
            });
          }
        }
      }
    }
  }, [lastJsonMessage])


  const buzzIn = () => {
    sendJsonMessage({
      type: events.BuzzerPressed
    });
  };

  let buzzerClass = "buzzer";
  if (playerState.buzzedIn) {
    buzzerClass += " buzzer-lit";
  }

  return (
   <div> 
    <br/><br/>
    <div className="podiumBackground">
      <div className="podiumPointsDisplay">
        <h1 className="podiumPointsText">{playerState.points}</h1>
      </div>
      <div className="buzzerHolder">
        <div className={buzzerClass} onClick={() => buzzIn()} />
      </div>
      <div className="podiumNameDisplay" >
        <h1 className="podiumNameText">{playerState.clientId}</h1>
      </div>
    </div>
    </div>
  );
};

export default Podium;