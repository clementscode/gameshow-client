import { BrowserRouter, Routes, Route, useNavigate } from "react-router-dom";
import { useCookies } from "react-cookie";
import GameDisplay from './GameDisplay';
import HostDisplay from './HostDisplay';
import Layout from './Layout';
import LoginPage from "./Login";
import RoomList from "./RoomList";
import './App.css';
import './join-room.css';
import axios, { AxiosError } from 'axios'
import { useEffect, useState } from "react";
import Podium from "./Podium";

function App() {
  const [cookies, setCookie, removeCookie] = useCookies(["usertoken"]);
  const [loggedIn, setLoggedIn] = useState(cookies.usertoken != null);

  const loginCallback = () => {
    setLoggedIn(true);
  };

  const logoutCallback = () => {
    removeCookie("usertoken");
    setLoggedIn(false);
  };
  
  return (
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout loggedIn={loggedIn} logoutCallback={logoutCallback} />}>
            <Route index element={<Home />} />
            <Route path="room/:roomId" element={<HostDisplay />} />
            <Route path="board/:roomId" element={<GameDisplay />} />
            <Route path="login" element={<LoginPage loginCallback={loginCallback} />} />
            <Route path="rooms" element={<RoomList />} />
            <Route path="podium/:roomId" element={<Podium />} />
            <Route path="*" element={<NoPage />} />
          </Route>
        </Routes>
      </BrowserRouter>
  );
}

const Home = () => {
  const [memberships, setMemberships] = useState({});
  const [roomId, setRoomId] = useState("");
  const [clientId, setClientId] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  const navigate = useNavigate();

  useEffect(() => {
    const getMemberships = async () => {
      try {
        const result = await axios.get("/api/memberships");
        setMemberships(result.data);
      } catch (err: AxiosError | any) {
        console.error(err)
      }
    };
    getMemberships();
  }, []);

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    const postPlayer = async () => {
      try {
        const result = await axios.post("/api/rooms/" + roomId, { id: clientId, role: "player" });
        navigate("/podium/" + roomId);
      } catch (err: AxiosError | any) {
        if (axios.isAxiosError(err)) {
          let message = "Error Response " + err.response?.statusText;
          switch (err.response?.status) {
            case 409:
              message = "Name taken, try another";
              break;
            case 404:
              message = "Room not found";
              break;
          }
          setErrorMessage(message);
        } else {
          console.error(err)
        }
      }
    };
    postPlayer();
  };

  const navToRoom = (roomId: string) => {
    navigate("/podium/" + roomId);
  };

  return (
    <div>
      {Object.keys(memberships).length > 0 &&
        <div>
          <h2 className="mainTitle">Rejoin Game</h2>
          <div className="joinRoom">
            <ul className="rejoinList">
              {Object.keys(memberships).map((roomId) => (
                <RejoinRoomItem key={roomId} roomId={roomId} onSelect={navToRoom} />
              ))}
            </ul>
          </div>
        </div>
      }
      <h2 className="mainTitle">Enter Room Number</h2>
      <div className="joinRoom">
        <form id="joinRoom" onSubmit={handleSubmit}>
          <label className="roomLabel"><b>Room Number</b>
            <input type="text" id="RoomNum" value={roomId} onChange={event => setRoomId(event.target.value)} />
            <br /><br />
          </label>
          <label className="roomLabel"><b>Player Name</b>
            <input type="text" id="PlayerName" value={clientId} onChange={event => setClientId(event.target.value)} />
            <br /><br />
          </label>
          <input type="submit" className="joinButton" value="Join" />
          {errorMessage && <p style={{ color: 'red' }}>{errorMessage}</p>}
        </form>
      </div>
    </div>
  );
};

type RejoinRoomProps = {
  roomId: string;
  onSelect: (roomId: string) => void;
};

const RejoinRoomItem = ({ roomId, onSelect }: RejoinRoomProps) => (
  <li className="rejoinRoomItem">
    <span>
      <button type="button" className="rejoinButton" onClick={() => onSelect(roomId)}>
        Rejoin {roomId}
      </button>
    </span>
  </li>
);

const NoPage = () => {
  return <h1>404 Page Not Found</h1>
};

export default App;
