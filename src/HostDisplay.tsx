import { useEffect, useState, useReducer } from 'react';
import useWebSocket from 'react-use-websocket';
import { useMemberInfo } from "./Tokens";
import { useParams, useNavigate } from 'react-router-dom';
import { Prompt, Game, useGameSocket, useGameData, Player, applyPlayerState, withUpdate, withUpdates, PlayerMap } from './GameData';
import * as events from './Events'
import axios from 'axios';

type HostState = {
    currentPrompt: number,
    players: PlayerMap
};

const getSortedPlayers = (players: Map<String, Player>): Array<Player> => {
  let buzzIndex = -1;
  let results: Array<Player> = Array.from(players.values());
  return results.sort((a: Player, b: Player) => {
    let aStageVal = a.onStage ? 1 : 0;
    let bStageVal = b.onStage ? 1 : 0;
    let diff = bStageVal - aStageVal;
    return diff !== 0 ? diff : a.name.localeCompare(b.name);
  });
};

const HostDisplay = () => {
    const navigate = useNavigate();
    const hostRole = "host";
    const { roomId } = useParams();
    if (!roomId) {
        navigate("/404");
    }
    const [game, isLoading, errorMessage] = useGameData(roomId);

    const [hostState, setHostState] = useState<HostState>({
        currentPrompt: -1,
        players: new Map<string, Player>()
    });

    const [isConnected, sendJsonMessage, lastJsonMessage] = useGameSocket(
        roomId || "", hostRole
    );

    useEffect(() => {
        if (lastJsonMessage != null) {
            let message = lastJsonMessage as events.EventMessage;
            switch (message.type) {
                case events.RoomState: {
                    let stateEvent = message.event as events.RoomStateEvent;
                    console.log("got room state: " + stateEvent.players);
                    setHostState({
                        ...hostState,
                        currentPrompt: stateEvent.promptIndex,
                        players: applyPlayerState(hostState.players, stateEvent.players),
                    })
                    break;
                }
                case events.ChangePrompt: {
                    let change = message.event as events.ChangePromptEvent;
                    setHostState({
                        ...hostState,
                        currentPrompt: change.promptIndex
                    });
                    break;
                }
                case events.BuzzerAccepted: {
                    setHostState({
                        ...hostState,
                        players: withUpdate(hostState.players, message.subject, (player) => {
                            player.buzzedIn = true;
                        })
                    });
                    break;
                }
                case events.JoinedRoom: {
                    let player = {name: message.subject, points: 0, onStage: true, buzzedIn: false};
                    setHostState({
                        ...hostState,
                        players: new Map(hostState.players).set(player.name, player)
                    });
                    break;
                };
            }
        }
    }, [lastJsonMessage])

    const updatePrompt = (promptIndex: number, resetBuzzer: boolean) => {
        sendJsonMessage({
            type: events.ChangePrompt,
            event: {
                promptIndex: promptIndex
            }
        });
        if (resetBuzzer) {
            clearBuzzer();
        }
    };

    const onPoints = (clientId: string, change: number) => {
        sendJsonMessage({
            type: events.ChangePoints,
            subject: clientId,
            event: {
                change: change,
            }
        });
        setHostState({
            ...hostState,
            players: withUpdate(hostState.players, clientId, (player) => {
                player.points += change;
            })
        });
    };

    const onPresenceChange = (clientId: string, onStage: boolean) => {
        sendJsonMessage({
            type: events.SetStagePresence,
            subject: clientId,
            event: {
                onStage: onStage,
            }
        });
        setHostState({
            ...hostState,
            players: withUpdate(hostState.players, clientId, (player) => {
                player.onStage = onStage;
            })
        });
    };

    const clearBuzzer = () => {
        sendJsonMessage({
            type: events.BuzzerReset
        });
        setHostState({
            ...hostState,
            players: withUpdates(hostState.players, (player) => {
              player.buzzedIn = false;  
            })
        });
    };
    
    const endGame = () => {
        sendJsonMessage({
            type: events.EndGame
        });
    };

    let answerDisp;
    if (game && !isLoading && !errorMessage && isConnected) {
        answerDisp = <AnswerDisplay data={game}
            currentPrompt={hostState.currentPrompt} onSwitchPrompt={updatePrompt} />
    } else if (isLoading || !isConnected) {
        answerDisp = <p>Loading...</p>
    } else if (errorMessage) {
        answerDisp = <p>{errorMessage}</p>
    }
    return (
        <div>
            {answerDisp}
            <br />
            {roomId &&
                <a href={"/board/" + roomId}
                    target="_blank" rel="noreferrer">Open Game Display</a>
            }
            <br />
            <PlayerList players={getSortedPlayers(hostState.players)} onPoints={onPoints} onPresenceChange={onPresenceChange}/>
            <br />
            <button type="button" onClick={() => clearBuzzer()}>
                Clear Buzzer
            </button>
            <button type="button" onClick={() => endGame()}>
                End Game
            </button>
        </div>
    );
};


type AnswerDisplayProps = {
    data: Game;
    currentPrompt: number;
    onSwitchPrompt: (count: number, resetBuzzer: boolean) => void;
};

const AnswerDisplay = ({ data, currentPrompt, onSwitchPrompt }: AnswerDisplayProps) => {
    let promptText = data.name;
    let promptAnswer = "";
    let nextDisabled = false;
    let prevDisabled = true;
    if (currentPrompt >= 0) {
        let prompt = data.prompts[currentPrompt];
        promptText = prompt.text;
        promptAnswer = prompt.correctResponse;
        nextDisabled = currentPrompt >= data.prompts.length - 1;
        prevDisabled = false;
    }
    return (
        <div>
            <div className="hostPrompt">
                <p className="hostPromptText">{promptText}</p>
            </div>
            <div className="hostPrompt">
                <p className="hostPromptText">{promptAnswer}</p>
            </div>
            <button type="button" onClick={() => onSwitchPrompt(currentPrompt - 1, false)} disabled={prevDisabled}>
                Previous
            </button>
            <button type="button" onClick={() => onSwitchPrompt(currentPrompt + 1, false)} disabled={nextDisabled}>
                Next
            </button>
            <button type="button" onClick={() => onSwitchPrompt(currentPrompt + 1, true)} disabled={nextDisabled}>
                Next and reset
            </button>
        </div>
    );
}

type PlayerListProps = {
    players: Array<Player>;
    onPoints: (clientId: string, change: number) => void;
    onPresenceChange: (clientId: string, onStage: boolean) => void;
}

const PlayerList = ({ players, onPoints, onPresenceChange }: PlayerListProps) => (
    <ul>
        {players.map((item) => (
            <PlayerItem
                key={item.name}
                item={item}
                onPoints={onPoints}
                onPresenceChange={onPresenceChange}
            />
        ))}
    </ul>
);

type PlayerItemProps = {
    item: Player;
    onPoints: (clientId: string, change: number) => void;
    onPresenceChange: (clientId: string, onStage: boolean) => void;
};

const PlayerItem = ({ item, onPoints, onPresenceChange }: PlayerItemProps) => (
    <li className="player">
        {item.name + " | "}
        {item.points + " "}
        <span>
            <button type="button" onClick={() => onPoints(item.name, -1)}>
                -
            </button>
        </span>
        <span>
            <button type="button" onClick={() => onPoints(item.name, 1)}>
                +
            </button>
        </span>
        <span>
            <button type="button" onClick={() => onPresenceChange(item.name, !item.onStage)}>
                {item.onStage ? "Kick off stage" : "Add to stage"}
            </button>
        </span>
        {item.buzzedIn && <span>   BUZZED!</span>}
    </li>
);

export default HostDisplay;