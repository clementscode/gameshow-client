import * as React from 'react';
import axios, { AxiosError } from 'axios'
import { useNavigate } from "react-router-dom";

import './RoomList.css';

type RoomInfo = {
    id: string;
    host: string;
    game: string;
};

type RoomInfoList = Array<RoomInfo>;

const listToMap = (list: RoomInfoList): Map<string, RoomInfo> => {
    const rval = new Map<string, RoomInfo>();
    list.forEach((room) => {
        rval.set(room.id, room);
    });
    return rval;
};

type RoomListState = {
    rooms: Map<string, RoomInfo>;
    games: Array<string>;
    isLoading: boolean;
    isError: boolean;
    errorMessage: string;
};

interface FetchStartAction {
    type: "FETCH_START";
}

interface FetchSuccessAction {
    type: "FETCH_SUCCESS";
    roomList: RoomInfoList;
    gameList: Array<string>;
}

interface FetchFailureAction {
    type: "FETCH_FAILURE";
    message: string;
}

interface RoomAddedAction {
    type: "ROOM_ADDED";
    payload: RoomInfo;
}

interface RoomAddFailedAction {
    type: "ROOM_ADD_FAILED";
    message: string;
}

interface RoomDeletedAction {
    type: "ROOM_DELETED";
    roomId: string;
}

type RoomListAction =
    | FetchStartAction
    | FetchSuccessAction
    | FetchFailureAction
    | RoomAddedAction
    | RoomAddFailedAction
    | RoomDeletedAction;

function filterMap<K, V> (
    m: Map<K, V>, 
    filterFunc: (k: K, v: V) => boolean
): Map<K, V> {
    let filtered = new Map<K, V>();
    m.forEach((value: V, key: K) => {
        if (filterFunc(key, value)) {
            filtered.set(key, value);
        }
    });
    return filtered;
};

const roomListReducer = (
    state: RoomListState,
    action: RoomListAction
) => {
    switch (action.type) {
        case "FETCH_START":
            return {
                ...state,
                isLoading: true,
                isError: false,
            };
        case "FETCH_SUCCESS":
            return {
                ...state,
                isLoading: false,
                isError: false,
                rooms: listToMap(action.roomList),
                games: action.gameList,
            };
        case "FETCH_FAILURE":
            return {
                ...state,
                isLoading: false,
                isError: true,
                errorMessage: action.message,
            };
        case "ROOM_ADDED":
            return {
                ...state,
                isLoading: false,
                isError: false,
                rooms: state.rooms.set(action.payload.id, action.payload),
            };
        case 'ROOM_ADD_FAILED':
            return {
                ...state,
                isLoading: false,
                isError: true,
                errorMessage: action.message,
            };
            case 'ROOM_DELETED':
                return {
                    ...state,
                    rooms: filterMap(state.rooms, (k, v) => v.id != action.roomId),
                }
        default:
            throw new Error("Invalid action type");
    }
};

const RoomList = () => {

    const [rooms, dispatchRooms] = React.useReducer(
        roomListReducer,
        {
            rooms: new Map<string, RoomInfo>, games: [],
            isLoading: false, isError: false, errorMessage: ""
        }
    );

    React.useEffect(() => {
        dispatchRooms({ type: "FETCH_START" });
        const fetchData = async () => {
            let roomResult;
            let gameResult;
            let errorMessage = "";
            try {
                roomResult = await axios("/api/rooms");
            } catch (error) {
                console.error(error);
                errorMessage = "Problem loading room info. ";
            }
            try {
                gameResult = await axios("/api/gamedata");
            } catch (error) {
                console.error(error);
                errorMessage += "Problem loading game list. ";
            }
            if (errorMessage) {
                dispatchRooms({ type: "FETCH_FAILURE", message: errorMessage });
            } else {
                dispatchRooms({ type: "FETCH_SUCCESS", roomList: roomResult?.data, gameList: gameResult?.data });
            }
        };
        fetchData();
    }, []);

    const handleNewRoom = (gameId: string) => {
        const postRoom = async () => {
            try {
                const result = await axios.post("/api/rooms", { game: gameId });
                dispatchRooms({ type: 'ROOM_ADDED', payload: result.data });
            } catch (err: AxiosError | any) {
                if (axios.isAxiosError(err)) {
                    let message = "Error Response " + err.response?.statusText;
                    switch (err.response?.status) {
                        case 403:
                            message = "Authorization failure";
                            break;
                        case 500:
                            message = "Server Error";
                            break;
                    }
                } else {
                    console.error(err)
                }
            }
        };
        postRoom();
    };

    let navigate = useNavigate();
    const handleLaunch = (roomId: string, role: string) => {
        if (role === "display") {
            const postDisplay = async () => {
                try {
                    const result = await axios.post("/api/rooms/" + roomId, { role: "display" });
                    navigate("/board/" + roomId);
                } catch (err: AxiosError | any) {
                    console.error(err)
                }
            };
            postDisplay();
        } else {
            navigate("/room/" + roomId);
        }
    };

    const handleDelete = (roomId: string) => {
        axios.delete("/api/rooms/" + roomId)
          .then(response => dispatchRooms({type: "ROOM_DELETED", roomId: roomId}))
          .catch(error => console.error(error));
    };

    return (
        <div>
            {rooms.isError && <p>{rooms.errorMessage}</p>}
            <Controls games={rooms.games} onAddRoom={handleNewRoom} />
            <List roomMap={rooms.rooms} onSelect={handleLaunch} onDelete={handleDelete} />
        </div>
    );
};

type ControlsProps = {
    games: Array<string>;
    onAddRoom: (game: string) => void;
};

const Controls = ({ games, onAddRoom }: ControlsProps) => {

    const [gameId, setGameId] = React.useState("");
    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        onAddRoom(gameId);
    };

    return (
        <form onSubmit={handleSubmit}>
            <select value={gameId !== "" && gameId || "Games"} onChange={event => setGameId(event.target.value)}>
                <option disabled hidden>Games</option>
                {games.map((id) =>
                    <option key={id} value={id}>
                        {id}
                    </option>
                )}
            </select>
            <input type="submit" value="Create Room" disabled={!gameId} />
        </form>
    );
};

type ListProps = {
    roomMap: Map<string, RoomInfo>;
    onSelect: (roomId: string, role: string) => void;
    onDelete: (roomId: string) => void;
}

const List = ({roomMap, onSelect, onDelete}: ListProps) => (
    <ul>
        {Array.from(roomMap.values(), (item) => (
            <Item
                key={item.id}
                item={item}
                onSelect={onSelect}
                onDelete={onDelete}
                />
        ))}
    </ul>
);

type ItemProps = {
    item: RoomInfo;
    onSelect: (roomId: string, role: string) => void;
    onDelete: (roomId: string) => void;
};

const Item = ({item, onSelect, onDelete}: ItemProps) => (
    <li className="room">
        <span className="roomLink" onClick={event=>onSelect(item.id, "host")}>
            {item.id}
        </span>
        <span>{item.game}</span>
        <span>{item.host}</span>
        <span>
            <button type="button" onClick={() => onSelect(item.id, "host")}>
                Host
            </button>
        </span>
        <span>
            <button type="button" onClick={() => onSelect(item.id, "display")}>
                Open Display
            </button>
        </span>
        <span>
            <button type="button" onClick={() => onDelete(item.id)}>
                Delete
            </button>
        </span>
    </li>
);

export default RoomList;