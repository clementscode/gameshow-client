import { useEffect, useState } from 'react';
import { WebSocketHook, JsonValue, SendJsonMessage } from 'react-use-websocket/dist/lib/types';
import useWebSocket from 'react-use-websocket';
import * as events from './Events'
import axios from 'axios';
import { isConstTypeReference } from 'typescript';

export type Prompt = {
  text: string;
  correctResponse: string;
  imageFile: string;
};

export type Prompts = Array<Prompt>;

export type Game = {
  type: string;
  name: string;
  gameId: string;
  prompts: Prompts;
};

export const useGameData = (roomId: string | null | undefined): [Game, boolean, string] => {
  const [game, setGame] = useState<Game>({
    gameId: "",
    name: "",
    prompts: [],
    type: "",
  });
  const [errorMessage, setErrorMessage] = useState("");
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    setLoading(true);
    const fetchData = async () => {
      try {
        const result = await axios("/api/rooms/" + roomId + "/data");
        setGame(result.data);
      } catch (error) {
        console.error(error);
        setErrorMessage("Problem fetching game data");
      }
      setLoading(false);
    };
    if (roomId) {
      fetchData();
    } else {
      setErrorMessage("Missing room ID");
    }
  }, []);

  return [game, loading, errorMessage];
}

export const useGameSocket = (roomId: string, role: string, firstMessage?: events.EventMessage): [boolean, SendJsonMessage, JsonValue | null] => {
  const [socketConnected, setSocketConnected] = useState(false);
  const wsProtocol = window.location.protocol === "https:" ? "wss://" : "ws://";
  const { sendJsonMessage, lastJsonMessage } = useWebSocket(wsProtocol + window.location.host + "/ws/" + roomId + "/" +role, {
    onOpen: () => {
      setSocketConnected(true);
      if (firstMessage) {
        sendJsonMessage(firstMessage);
      }
    },
    onError: (event: WebSocketEventMap['error']) => {
      console.error("Websocket error:" + JSON.stringify(event));
    },
    onClose: (event: WebSocketEventMap['close']) => {
      console.log("Websocket close:" + JSON.stringify(event));
      setSocketConnected(false);
    },
    retryOnError: true,
    reconnectAttempts: 10000,
  });
  return [socketConnected, sendJsonMessage, lastJsonMessage];
};

export type Player = {
    name: string;
    points: number;
    onStage: boolean;
    buzzedIn: boolean;
};

export type PlayerMap = Map<string, Player>;

export const withUpdate = (
    state: PlayerMap,
    name: string,
    updateFunc: (p: Player) => void ): PlayerMap => {
    let player = state.get(name); 
    if (player) {
        player = {...player};
        updateFunc(player);
        return new Map(state).set(name, player);
    }
    return state;
};

export const withUpdates = (
    state: PlayerMap,
    updateFunc: (p: Player) => void ): PlayerMap => {
    let updates = new Map<string, Player>();
    state.forEach((player, name) => {
        let updated = {...player};
        updateFunc(updated);
        updates.set(name, updated);
    });
    return updates;
};

export const applyPlayerState = (
    state: PlayerMap, 
    players: Array<events.PlayerStateEvent>): PlayerMap => {
    let updates = new Map<string, Player>(state);
    if (players === null ) {
      return state
    }
    for (let event of players ) {
        let player = updates.get(event.clientId);
        if (player) {
            player.buzzedIn = event.buzzedIn;
            player.points = event.points;
            player.onStage = event.onStage;
        } else {
            updates.set(event.clientId, {
                name: event.clientId,
                points: event.points,
                onStage: event.onStage,
                buzzedIn: event.buzzedIn,
            });
        }
    }
    return updates;
};